# Automation priority: null
# Test case importance: Very high
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_5_SETUP}	Get Variable Value	${TEST 5 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_5_SETUP is not None	${__TEST_5_SETUP}

Test Teardown
	${__TEST_5_TEARDOWN}	Get Variable Value	${TEST 5 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_5_TEARDOWN is not None	${__TEST_5_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Connexion Invalide
	${url} =	Get Test Param	DS_url
	${mot_de_passe} =	Get Test Param	DS_mot_de_passe
	${login} =	Get Test Param	DS_login

	[Setup]	Test Setup

	Given Je suis sur la page de connexion : ${url}
	When Je rentre une mauvaise combinaison ${login}/${mot_de_passe}
	Then Un message "Echec d'authentification" apparaît
	And Je ne suis pas connecté(e)

	[Teardown]	Test Teardown
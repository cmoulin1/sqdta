# Automation priority: null
# Test case importance: Very high
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_4_SETUP}	Get Variable Value	${TEST 4 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_4_SETUP is not None	${__TEST_4_SETUP}

Test Teardown
	${__TEST_4_TEARDOWN}	Get Variable Value	${TEST 4 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_4_TEARDOWN is not None	${__TEST_4_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Connexion Valide
	${url} =	Get Test Param	DS_url
	${mot_de_passe} =	Get Test Param	DS_mot_de_passe
	${login} =	Get Test Param	DS_login

	[Setup]	Test Setup

	Given Je suis sur la page de connexion : ${url}
	When Je renseigne un ${login} et un ${mot_de_passe} valide
	Then Je suis connecté(e)

	[Teardown]	Test Teardown
# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_14_SETUP}	Get Variable Value	${TEST 14 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_14_SETUP is not None	${__TEST_14_SETUP}

Test Teardown
	${__TEST_14_TEARDOWN}	Get Variable Value	${TEST 14 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_14_TEARDOWN is not None	${__TEST_14_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Recherche
	[Setup]	Test Setup

	Given Je suis connecté(e) en tant qu'utilisateur
	When Je rentre "T-Shirt" dans la barre de recherche
	Then Une suggestion s'affiche sous la barre de recherche
	And Lorsque je clique dessus j'arrive sur l'affichage de l'objet avec sa description, sa photo, le prix et la possibilité d'ajouter l'objet au panier

	[Teardown]	Test Teardown